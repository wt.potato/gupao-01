package com.wtd.activemq.lesslon3;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Enumeration;

public class PrefetchSizeQueueConsumer {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.80.135:61616");
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            ((ActiveMQConnection)connection).setTransactedIndividualAck(true);
            connection.start();

            Session session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
            // 创建目的地
            Destination destination = session.createQueue("myQueue");
            //创建消费者
            MessageConsumer consumer = session.createConsumer(destination);
            //接收消息
            for (int i = 0; i < 1000; i++) {
                TextMessage message = (TextMessage) consumer.receive();
                System.out.println(message.getText());
                Thread.sleep(1000);
            }

            session.close();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
          if (connection != null) {
              try {
                  connection.close();
              } catch (JMSException e) {
                  e.printStackTrace();
              }
          }
        }
    }
}
