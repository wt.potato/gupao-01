package com.wtd.activemq;

import org.apache.activemq.ActiveMQConnection;
import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Enumeration;

public class JMSQueueConsumer {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.80.135:61616");
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            ((ActiveMQConnection)connection).setTransactedIndividualAck(true);
            connection.start();

            /*
            * 事务型会话  Boolean.TRUE
            * Session.AUTO_ACKNOWLEDGE 自动确认
            * 客户端无须手动签收，但必须保证发送端与接收端都是事务型会话
            * 非事务型会话 Boolean.FALSE
            * Session.AUTO_ACKNOWLEDGE 自动确认
            * Session.CLIENT_ACKNOWLEDGE 客户端确认
            * Session.DUPS_OK_ACKNOWLEDGE 延迟确认，指定消息提供者在消息接收者没有确认发送时重新发送消息，这种模式不在乎接受者收到重复的消息
             */
            //Session.SESSION_TRANSACTED  ？？？
            Session session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
            // 创建目的地
            Destination destination = session.createQueue("myQueue");
            //创建消费者
            MessageConsumer consumer = session.createConsumer(destination);
            //接收消息
            TextMessage message = (TextMessage) consumer.receive();
            System.out.println(message.getText());

            //
//            Enumeration enumeration = message.getPropertyNames();
//            while (enumeration.hasMoreElements()) {
//                String name = enumeration.nextElement().toString();
//                System.out.println("name:" + name + ", value:" + message.getStringProperty(name));
//            }
//            message.acknowledge();//客户端确认签收

//            session.commit();   //非事务型会话无须提交
            session.close();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
          if (connection != null) {
              try {
                  connection.close();
              } catch (JMSException e) {
                  e.printStackTrace();
              }
          }
        }
    }
}
