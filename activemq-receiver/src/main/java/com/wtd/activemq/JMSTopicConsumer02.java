package com.wtd.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;
import java.util.Enumeration;

public class JMSTopicConsumer02 {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory = new ActiveMQConnectionFactory("tcp://192.168.139.133:61616");
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();

            //Session.AUTO_ACKNOWLEDGE 消息自动确认
            Session session = connection.createSession(Boolean.TRUE, Session.AUTO_ACKNOWLEDGE);
            // 创建目的地
            Destination destination = session.createTopic("myTopic");
            //创建消费者
            MessageConsumer consumer = session.createConsumer(destination);
            //接收消息
            TextMessage message = (TextMessage) consumer.receive();
            System.out.println(message.getText());

            //
            Enumeration enumeration = message.getPropertyNames();
            while (enumeration.hasMoreElements()) {
                String name = enumeration.nextElement().toString();
                System.out.println("name:" + name + ", value:" + message.getStringProperty(name));
            }

            session.commit();
            session.close();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
          if (connection != null) {
              try {
                  connection.close();
              } catch (JMSException e) {
                  e.printStackTrace();
              }
          }
        }
    }
}
