package com.wtd.activemq;

import org.apache.activemq.ActiveMQConnectionFactory;

import javax.jms.*;

public class JMSQueueListenerConsumer {

    public static void main(String[] args) {
        ConnectionFactory connectionFactory =
                new ActiveMQConnectionFactory("tcp://192.168.80.135:61616");//?jms.redeliveryPolicy.maximumRedeliveries=2
        Connection connection = null;
        try {
            connection = connectionFactory.createConnection();
            connection.start();
            Session session = connection.createSession(Boolean.FALSE, Session.AUTO_ACKNOWLEDGE);
            // 创建目的地
            Destination destination = session.createQueue("myQueue");
            //创建消费者
            MessageConsumer consumer = session.createConsumer(destination);
            //消费者监听
            MessageListener messageListener = new MessageListener() {
                @Override
                public void onMessage(Message message) {
                    try {
                        System.out.println(((TextMessage)message).getText());
//                        Enumeration enumeration = message.getPropertyNames();
//                        while (enumeration.hasMoreElements()) {
//                            String name = enumeration.nextElement().toString();
//                            System.out.println("name:" + name + ", value:" + message.getStringProperty(name));
//                        }
//                        throw new RuntimeException("occur exception");
                    } catch (JMSException e) {
                        e.printStackTrace();
                    }
                }
            };
            //添加消息监听
            consumer.setMessageListener(messageListener);
            System.in.read();

//            session.commit();
            session.close();
        } catch (Exception e){
            e.printStackTrace();
        } finally {
          if (connection != null) {
              try {
                  connection.close();
              } catch (JMSException e) {
                  e.printStackTrace();
              }
          }
        }
    }
}
