package com.wtd.rpc;


import com.wtd.rpc.zk.IServiceDiscovery;

import java.lang.reflect.Proxy;

/**
 * @author wt.d
 * @date 14:19 2018/6/13
 */
public class RpcClientProxy {

    private IServiceDiscovery serviceDiscovery;

    public RpcClientProxy(IServiceDiscovery serviceDiscovery) {
        this.serviceDiscovery = serviceDiscovery;
    }

    /**
     * 创建客户端的远程代理，通过远程代理进行访问
     * @param interfaceCls
     * @param version
     * @param <T>
     * @return
     */
    public <T> T clientProxy(Class<T> interfaceCls, String version){
        //使用动态代理
        return (T) Proxy.newProxyInstance(interfaceCls.getClassLoader(),
                new Class[]{interfaceCls}, new RemoteInvocationHandler(serviceDiscovery, version));
    }
}
