package com.wtd.rpc.zk.loadbalance;

import java.util.List;
import java.util.Random;

/**
 * @author wt.d
 * @date 14:09 2018/6/13
 */
public class RandomLoadBanalce implements LoadBanalce {

    @Override
    public String selectHost(List<String> repos) {
        int len = repos.size();
        Random random = new Random();
        return repos.get(random.nextInt(len));
    }
}
