package com.wtd.rpc.zk;

/**
 * @author wt.d
 * @date 11:35 2018/6/13
 */
public interface IServiceDiscovery {

    String diccovery(String serviceName);
}
