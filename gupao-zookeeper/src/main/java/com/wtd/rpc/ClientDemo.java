package com.wtd.rpc;


import com.wtd.rpc.zk.Conf;
import com.wtd.rpc.zk.IServiceDiscovery;
import com.wtd.rpc.zk.ServiceDiscoveryImpl;

/**
 * @author wt.d
 * @date 11:32 2018/6/13
 */
public class ClientDemo {

    public static void main(String[] args) throws InterruptedException {
        IServiceDiscovery serviceDiscovery =
                new ServiceDiscoveryImpl(Conf.connectString);

        RpcClientProxy rpcClientProxy = new RpcClientProxy(serviceDiscovery);
        for (int i = 0; i < 5; i++) {
            IGpHello hello = rpcClientProxy.clientProxy(IGpHello.class, "2.0");
            System.out.println(hello.sayhello("mic"));
            Thread.sleep(1000);
        }
    }
}
