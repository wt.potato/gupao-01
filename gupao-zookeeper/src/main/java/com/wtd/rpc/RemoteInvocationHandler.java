package com.wtd.rpc;


import com.wtd.rpc.zk.IServiceDiscovery;

import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;

/**
 * @author wt.d
 * @date 14:23 2018/6/13
 */
public class RemoteInvocationHandler implements InvocationHandler {

    private IServiceDiscovery serviceDiscovery;
    private String version;

    public RemoteInvocationHandler(IServiceDiscovery serviceDiscovery, String version) {
        this.serviceDiscovery = serviceDiscovery;
        this.version = version;
    }

    @Override
    public Object invoke(Object proxy, Method method, Object[] args) throws Throwable {
        //组装请求
        RpcRequest request = new RpcRequest();
        request.setClassName(method.getDeclaringClass().getName());
        request.setMethodName(method.getName());
        request.setParameters(args);
        request.setVersion(version);

        String serviceAddress = serviceDiscovery.diccovery(request.getClassName());//接口名称对应的服务地址
        //通过TCP传输协议进行传输
        TCPTransport tcpTransport = new TCPTransport(serviceAddress);
        //发起请求
        return tcpTransport.send(request);
    }
}
