package com.wtd;

import com.wtd.dubbo.IDemoService;

/**
 * @author wt.d
 * @date 16:14 2018/6/22
 */
public class TestMock implements IDemoService {

    @Override
    public String protocolDemo(String s) {
        return "系统繁忙，" + s;
    }
}
