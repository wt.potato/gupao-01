package com.wtd;

import com.wtd.dubbo.IDemoService;
import com.wtd.dubbo.IGpHello;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import java.io.IOException;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ) throws IOException, InterruptedException {
        ClassPathXmlApplicationContext context = new ClassPathXmlApplicationContext("dubbo-client.xml");
        IGpHello gpHello = null;
        for (int i = 0; i < 1; i++) {
            gpHello = (IGpHello) context.getBean("gpHelloService");
            System.out.println(gpHello.sayHello("mic"));
            Thread.sleep(1000);
        }

        IDemoService demoService = null;
        demoService = (IDemoService) context.getBean("demoService");
        System.out.println(demoService.protocolDemo("wt.d"));

//        System.in.read();//阻塞
    }
}
